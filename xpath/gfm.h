/* GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */
#ifndef __GFM_H__
#define __GFM_H__

#include <gtk/gtk.h>
#include <freetype.h>
#include <gnome-xml/parser.h>

#include "gfmface.h"

#
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GFM_TYPE_GFM            (gfm_get_type ())
#define GFM(obj)                (GTK_CHECK_CAST ((obj), GFM_TYPE_GFM, GFontManager))
#define GFM_CLASS(klass)        (GTK_CHECK_CLASS_CAST ((klass), GFM_TYPE_GFM, GFontManagerClass))
#define GFM_IS_GFM(obj)         (GTK_CHECK_TYPE ((obj), GFM_TYPE_GFM))
#define GFM_IS_GFM_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GFM_TYPE_GFM))

typedef struct _GFontManager       GFontManager;
typedef struct _GFontManagerClass  GFontManagerClass;

struct _GFontManager
{
  GtkObject   object;
  
  FT_Library  library;
  xmlDoc     *doc;
};

GtkType        gfm_get_type        (void);
GFontManager * gfm_new             (void);

void           gfm_fontfile_add    (GFontManager *gfm, 
				    gchar        *filename);
void           gfm_fontfile_remove (GFontManager *gfm, 
				    gchar        *filename);
  
gboolean       gfm_rcfile_load     (GFontManager *gfm, 
				    gchar        *filename);
gboolean       gfm_rcfile_save     (GFontManager *gfm, 
				    gchar        *filename);

GList *        gfm_face_lookup     (GFontManager *gfm, 
				    gchar        *family_name,
				    gchar        *style_name);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GFM_H__ */





