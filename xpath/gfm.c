/* GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */
#include "config.h"

#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <gnome-xml/parser.h>

#include "gfm.h"
#include "gfmrc.h"



struct _GFontManagerClass
{
  GtkObjectClass  parent_class;
};  


static void gfm_destroy     (GtkObject    *object);

static void gfm_face_add    (GFontManager *gfm,
			     FT_Face       ft_face,
			     gchar        *filename,
			     gint          index);
static void gfm_face_remove (GFontManager *gfm,
			     FT_Face       ft_face,
			     gchar        *filename,
			     gint          index);


static GtkObjectClass *parent_class = NULL;


static void 
gfm_destroy (GtkObject *object)
{
  GFontManager *gfm;

  g_return_if_fail (gfm = GFM (object));

  if (gfm->library)
    FT_Done_FreeType (gfm->library);

  if (gfm->doc)
    xmlFreeDoc (gfm->doc);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gfm_class_init (GFontManagerClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;

  parent_class = gtk_type_class (gtk_object_get_type ());

  object_class->destroy = gfm_destroy;
}

static void
gfm_init (GFontManager *gfm)
{
  gfm->library = NULL;
  gfm->doc     = NULL;
}

GtkType
gfm_get_type (void)
{
  static guint gfm_type = 0;

  if (!gfm_type)
    {
      GtkTypeInfo gfm_info =
      {
	"GFontManager",
	sizeof (GFontManager),
	sizeof (GFontManagerClass),
	(GtkClassInitFunc) gfm_class_init,
	(GtkObjectInitFunc) gfm_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL
      };

      gfm_type = gtk_type_unique (gtk_object_get_type (), &gfm_info);
    }

  return gfm_type;
}

GFontManager *
gfm_new (void)
{
  GFontManager *gfm;
  FT_Error error;

  gfm = gtk_type_new (gfm_get_type ());

  error = FT_Init_FreeType (&gfm->library);
  if (error)
    {
      g_warning ("Couldn't initialize FreeType library");
      gfm_destroy (GTK_OBJECT (gfm));
      return NULL;
    }

  return gfm;
}

void        
gfm_fontfile_add (GFontManager *gfm,
		  gchar        *filename)
{
  struct stat filestat;
  gint i;
  FT_Face  face;
  FT_Error error;

  g_return_if_fail (GFM_IS_GFM (gfm));
  g_return_if_fail (filename != NULL);

  if (stat (filename, &filestat) || access (filename, R_OK))
    {
      g_warning ("cannot read file %s", filename);

      return;
    }

  error = FT_New_Face (gfm->library, filename, 0, &face);
  
  if (!error)
    {
      gfm_face_add (gfm, face, filename, 0);

      for (i = 1; i < face->num_faces; i++)
	{
	  FT_Done_Face (face);
	  error = FT_New_Face (gfm->library, filename, i, &face);
	  if (!error)
	    gfm_face_add (gfm, face, filename, i);
	}
      
      FT_Done_Face (face);
    }
}


void        
gfm_fontfile_remove (GFontManager *gfm,
		     gchar        *filename)
{
  struct stat filestat;
  gint i;
  FT_Face  face;
  FT_Error error;

  g_return_if_fail (GFM_IS_GFM (gfm));
  g_return_if_fail (filename != NULL);

  if (stat (filename, &filestat) || access (filename, R_OK))
    {
      g_warning ("cannot read file %s", filename);

      return;
    }

  error = FT_New_Face (gfm->library, filename, 0, &face);
  
  if (!error)
    {
      gfm_face_remove (gfm, face, filename, 0);
      
      for (i = 1; i < face->num_faces; i++)
	{
	  FT_Done_Face (face);
	  error = FT_New_Face (gfm->library, filename, i, &face);
	  if (!error)
	    gfm_face_remove (gfm, face, filename, i);
	}
      
      FT_Done_Face (face);
    }
}

static void
gfm_face_add (GFontManager *gfm,
	      FT_Face       ft_face,
	      gchar        *filename,
	      gint          index)
{
  GfmFace *face;
  
  g_return_if_fail (GFM_IS_GFM (gfm));
  g_return_if_fail (ft_face != NULL);
  g_return_if_fail (filename != NULL);
  
  face = gfm_face_new (ft_face->family_name,
		       ft_face->style_name,
		       filename, index);
  gfm_rc_face_add (gfm, face);
  gtk_object_destroy (GTK_OBJECT (face));
}

	      
static void
gfm_face_remove (GFontManager *gfm,
		 FT_Face       ft_face,
		 gchar        *filename,
		 gint          index)
{
  GfmFace *face;
  
  g_return_if_fail (GFM_IS_GFM (gfm));
  g_return_if_fail (ft_face != NULL);
  g_return_if_fail (filename != NULL);
  
  face = gfm_face_new (ft_face->family_name,
		       ft_face->style_name,
		       filename, index);
  gfm_rc_face_remove (gfm, face);
  gtk_object_destroy (GTK_OBJECT (face));
}
