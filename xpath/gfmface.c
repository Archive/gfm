/* 
 * GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 * GfmFace
 *   An object describing some properties of a face and the location
 *   of the related font file.
 *
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */

#include "gfmface.h"

static void gfm_face_destroy      (GtkObject     *object);


static GtkObjectClass *parent_class = NULL;


static void 
gfm_face_destroy (GtkObject *object)
{
  GfmFace *face;
  
  g_return_if_fail (face = GFM_FACE (object));

  g_free (face->family_name);
  g_free (face->style_name);
  g_free (face->file_name);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gfm_face_class_init (GfmFaceClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;

  parent_class = gtk_type_class (gtk_object_get_type ());

  object_class->destroy = gfm_face_destroy;
}

static void
gfm_face_init (GfmFace *face)
{
  face->family_name = NULL;
  face->style_name  = NULL;
  face->file_name   = NULL;
  face->index       = 0;
}

GtkType
gfm_face_get_type (void)
{
  static guint gfm_face_type = 0;

  if (!gfm_face_type)
    {
      GtkTypeInfo gfm_face_info =
      {
	"GfmFace",
	sizeof (GfmFace),
	sizeof (GfmFaceClass),
	(GtkClassInitFunc) gfm_face_class_init,
	(GtkObjectInitFunc) gfm_face_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL
      };

      gfm_face_type = gtk_type_unique (gtk_object_get_type (), &gfm_face_info);
    }

  return gfm_face_type;
}

GfmFace *
gfm_face_new (gchar *family_name,
	      gchar *style_name, 
	      gchar *file_name, 
	      gint   index)
{
  GfmFace *face;
  
  g_return_val_if_fail (family_name != NULL, NULL);
  g_return_val_if_fail (style_name != NULL, NULL);
  g_return_val_if_fail (file_name != NULL, NULL);

  face = gtk_type_new (gfm_face_get_type ());

  face->family_name = g_strdup (family_name);
  face->style_name  = g_strdup (style_name);
  face->file_name   = g_strdup (file_name);
  face->index       = index;

  return face;
}

