/* GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 * GfmRC
 *   Read and write gfmrc files.
 *
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */
#include "config.h"

#include <gnome-xml/xpath.h>

#include "gfm.h"


gboolean
gfm_rcfile_load (GFontManager *gfm,
		 gchar        *filename)
{
  g_return_val_if_fail (GFM_IS_GFM (gfm), FALSE);
  g_return_val_if_fail (filename != NULL, FALSE);

  if (gfm->doc)
    xmlFreeDoc (gfm->doc);
  
  gfm->doc = xmlParseFile (filename); 
  
  if (gfm->doc == NULL)
    {
      g_warning ("Failure when parsing %s", filename);
      return FALSE;
    }

  return TRUE;
}


gboolean
gfm_rcfile_save (GFontManager *gfm,
		 gchar        *filename)
{
  g_return_val_if_fail (GFM_IS_GFM (gfm), FALSE);
  g_return_val_if_fail (filename != NULL, FALSE);

  if (!gfm->doc)
    {
      g_warning ("Tried to save empty document, ignored.");
      return TRUE;
    }
  
  return xmlSaveFile (filename, gfm->doc); 
}



#include <libxml/debugXML.h>

void xmlXPAthDebugDumpNodeSet(FILE *output, xmlNodeSetPtr cur) {
    int i;

    if (cur == NULL) {
	fprintf(output, "NodeSet is NULL !\n");
	return;
        
    }

    fprintf(output, "Set contains %d nodes:\n", cur->nodeNr);
    for (i = 0;i < cur->nodeNr;i++) {
        fprintf(output, "%d", i + 1);
	if (cur->nodeTab[i] == NULL)
	    fprintf(output, " NULL\n");
	else if ((cur->nodeTab[i]->type == XML_DOCUMENT_NODE) ||
	         (cur->nodeTab[i]->type == XML_HTML_DOCUMENT_NODE))
	    fprintf(output, " /\n");
	else if (cur->nodeTab[i]->type == XML_ATTRIBUTE_NODE)
	    xmlDebugDumpAttr(output, (xmlAttrPtr)cur->nodeTab[i], 2);
	else
	    xmlDebugDumpOneNode(output, cur->nodeTab[i], 2);
    }
}


#define EXISTING_FAMILY        "/gfm/user/family[@name='%s']"
#define USER_NODE              "/gfm/user"
#define LOOKUP_ALL             "/gfm/user/family/style"
#define LOOKUP_FAMILY          "/gfm/user/family[@name='%s']/style"
#define LOOKUP_STYLE           "/gfm/user/family/style[@name='%s']"
#define LOOKUP_STYLE_IN_FAMILY "/gfm/user/family[@name='%s']/style[@name='%s']"

#define DEBUG_EXPR(x) g_print ("XPath Expression: "); g_print (x); g_print ("\n");


void
gfm_rc_face_add (GFontManager *gfm,
		 GfmFace      *face)
{
  xmlXPathContext *context;
  xmlXPathObject  *family_object;
  xmlNodeSet      *nodeSet;
  xmlNode         *family_node;
  xmlNode         *style_node;
  gchar           *expr;

  g_return_if_fail (GFM_IS_GFM (gfm));
  g_return_if_fail (GFM_IS_FACE (face));
  g_return_if_fail (gfm->doc != NULL);

  context = xmlXPathNewContext (gfm->doc);

  expr = g_strdup_printf (EXISTING_FAMILY, face->family_name);
  DEBUG_EXPR (expr);
  family_object = xmlXPathEval ((xmlChar *) expr, context);
  g_free (expr);

  if (!family_object || family_object->type != XPATH_NODESET)
    g_error ("Expected nodeset, got %d", family_object->type);
  
  nodeSet = family_object->nodesetval;

  xmlXPAthDebugDumpNodeSet (stdout, nodeSet);

  if (nodeSet->nodeNr > 0)  /* family found */
    family_node = nodeSet->nodeTab[0];
  else
    {
      xmlXPathObject *result;

      result = xmlXPathEval (USER_NODE, context);
      if (!result || result->type != XPATH_NODESET)
	g_error ("Expected nodeset, got %d", result->type);
      
      nodeSet = result->nodesetval;
      
      xmlXPAthDebugDumpNodeSet (stdout, nodeSet);

      if (nodeSet->nodeNr == 0)
	g_error ("user_node not found");

      family_node = xmlNewNode (NULL, "family");
      xmlNewProp (family_node, "name", face->family_name);
      xmlAddChild (nodeSet->nodeTab[0], family_node);

      xmlXPathFreeObject (result);
    }

  style_node = xmlNewNode (NULL, "style");
  xmlNewProp (style_node, "name", face->style_name);
  xmlNewProp (style_node, "file", face->file_name);
  if (face->index != 0)
    {
      gchar *attr = g_strdup_printf ("%d", face->index);
      xmlNewProp (style_node, "index", attr);
      g_free (attr);
    }
  xmlAddChild (family_node, style_node);

  xmlXPathFreeObject (family_object);
  xmlXPathFreeContext (context); 
}


void
gfm_rc_face_remove (GFontManager *gfm,
		    GfmFace      *face)
{
  g_return_if_fail (GFM_IS_GFM (gfm));
  g_return_if_fail (GFM_IS_FACE (face));
  g_return_if_fail (gfm->doc != NULL);

  
}

GList *
gfm_face_lookup (GFontManager *gfm,
		 gchar        *family_name,
		 gchar        *style_name)
{
  xmlXPathContext *context;
  xmlXPathObject  *result;
  xmlNodeSet      *nodeSet;
  gchar           *expr;

  g_return_val_if_fail (GFM_IS_GFM (gfm), NULL);
  g_return_val_if_fail (gfm->doc != NULL, NULL);

  context = xmlXPathNewContext (gfm->doc);

  if (family_name && style_name)
    expr = g_strdup_printf (LOOKUP_STYLE_IN_FAMILY, family_name, style_name);
  else if (family_name)
    expr = g_strdup_printf (LOOKUP_FAMILY, family_name);
  else if (style_name)
    expr = g_strdup_printf (LOOKUP_STYLE, style_name);
  else
    expr = g_strdup_printf (LOOKUP_ALL);
  
  /* test only! */
  expr = "/gfm/user/family[@name='Agate']"; 
  // g_strdup_printf (EXISTING_FAMILY, family_name);
  DEBUG_EXPR (expr);
  result = xmlXPathEval((xmlChar *) expr, context);
  g_print ("xmlXPathEval done\n");
  //g_free (expr);

  if (!result || result->type != XPATH_NODESET)
    g_error ("Expected nodeset, got %d", result->type);

  nodeSet = result->nodesetval;
  g_print ("Before Dump\n");
  xmlXPAthDebugDumpNodeSet (stdout, nodeSet);
  g_print ("After Dump\n");

  xmlXPathFreeObject (result);
  xmlXPathFreeContext (context); 

  return NULL;
}

