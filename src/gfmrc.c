/* GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 * GfmRC
 *   Read and write gfmrc files.
 *
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */

#include "config.h"

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <gtk/gtk.h>

#include <gnome-xml/parser.h>
#include <gnome-xml/parserInternals.h>

#include "gfmtypes.h"

#include "gfm.h"
#include "gfmrc.h"


typedef enum 
{
  GFM_START,
  GFM_IN_FONTS,
  GFM_IN_FAMILY,
  GFM_IN_STYLE,
  GFM_IN_FILE,
  GFM_IN_INDEX,
  GFM_IN_TRIBE,
  GFM_FINISH,
  GFM_UNKNOWN
} GfmState;


typedef struct _GfmParser GfmParser;

struct _GfmParser
{
  gboolean   retval;
  GfmState   state;

  GString   *buffer;
  GString   *family_name;
  GString   *style_name;
  GString   *filename;
  guint      index;
  GSList    *tribes;

  GfmState   last_state;
  gint       unknown_depth;
};



static gboolean     gfm_rc_sax_parse_file (xmlSAXHandler *sax,
					   GfmParser     *parser,
					   const gchar   *filename);
static xmlEntity *  gfm_rc_getEntity      (GfmParser     *parser,
					   const CHAR    *name);
static void         gfm_rc_startDocument  (GfmParser     *parser);
static void         gfm_rc_endDocument    (GfmParser     *parser);
static void         gfm_rc_startElement   (GfmParser     *parser,
					   const CHAR    *name,
					   const CHAR   **attrs);
static void         gfm_rc_endElement     (GfmParser     *parser,
					   const CHAR    *name);
static void         gfm_rc_characters     (GfmParser     *parser,
					   const CHAR    *chars,
					   gint           len);
static void         gfm_rc_warning        (GfmParser     *parser,
					   const gchar   *msg,
					   ...);
static void         gfm_rc_error          (GfmParser     *parser,
					   const gchar   *msg,
					   ...);
static void         gfm_rc_fatal          (GfmParser     *parser,
					   const gchar   *msg,
					   ...);

static xmlSAXHandler gfmSAXHandler =
{
  NULL,  /*  internalSubsetSAXFunc         */
  NULL,  /*  isStandaloneSAXFunc           */
  NULL,  /*  hasInternalSubsetSAXFunc      */
  NULL,  /*  hasExternalSubsetSAXFunc      */
  NULL,  /*  resolveEntitySAXFunc          */
  (getEntitySAXFunc)     gfm_rc_getEntity,
  NULL,  /*  entityDeclSAXFunc             */
  NULL,  /*  notationDeclSAXFunc           */
  NULL,  /*  attributeDeclSAXFunc          */
  NULL,  /*  elementDeclSAXFunc            */
  NULL,  /*  unparsedEntityDeclSAXFunc     */
  NULL,  /*  setDocumentLocatorSAXFunc     */
  (startDocumentSAXFunc) gfm_rc_startDocument,
  (endDocumentSAXFunc)   gfm_rc_endDocument,
  (startElementSAXFunc)  gfm_rc_startElement,
  (endElementSAXFunc)    gfm_rc_endElement,
  NULL,  /*  referenceSAXFunc              */
  (charactersSAXFunc)    gfm_rc_characters,
  NULL,  /*  ignorableWhitespaceSAXFunc    */
  NULL,  /*  processingInstructionSAXFunc  */
  NULL,  /*  commentSAXFunc                */
  (warningSAXFunc)       gfm_rc_warning,
  (errorSAXFunc)         gfm_rc_error, 
  (fatalErrorSAXFunc)    gfm_rc_fatal,
  NULL,  /*  getParameterEntitySAXFunc     */
  NULL   /*  cdataBlockSAXFunc             */
};


void
gfm_rcfile_load (GFontManager *gfm,
		 gchar        *filename)
{
  struct stat filestat;
  GfmParser   parser;

  g_return_if_fail (GFM_IS_GFM (gfm));
  g_return_if_fail (filename != NULL);
  
  if (stat (filename, &filestat) || access (filename, R_OK))
    {
      g_warning ("cannot read file %s", filename);
      return;
    }
  
  gfm_rc_sax_parse_file (&gfmSAXHandler, &parser, filename);
}


static gboolean
gfm_rc_sax_parse_file (xmlSAXHandler *sax,
		       GfmParser     *parser, 
		       const gchar   *filename) 
{
  gboolean retval;
  xmlParserCtxt *ctxt;

  ctxt = xmlCreateFileParserCtxt (filename);
  if (ctxt == NULL) 
    return FALSE;

  ctxt->sax = sax;
  ctxt->userData = parser;
  
  xmlParseDocument(ctxt);

  if (ctxt->wellFormed)
    retval = TRUE;
  else
    retval = FALSE;

  if (sax != NULL)
    ctxt->sax = NULL;
  
  xmlFreeParserCtxt (ctxt);
  
  return retval;
}

static xmlEntity *  
gfm_rc_getEntity (GfmParser  *parser,
		  const CHAR *name)
{
  return xmlGetPredefinedEntity (name);
}

static void         
gfm_rc_startDocument (GfmParser *parser)
{
  parser->retval        = TRUE;
  parser->state         = GFM_START;
  parser->buffer        = g_string_new (NULL);
  parser->family_name   = g_string_new (NULL);
  parser->style_name    = g_string_new (NULL);
  parser->filename      = g_string_new (NULL);
  parser->index         = 0;
  parser->tribes        = NULL;
  parser->unknown_depth = 0;
}

static void         
gfm_rc_endDocument (GfmParser *parser)
{
  GSList *list;

  g_string_free (parser->buffer, TRUE);
  g_string_free (parser->family_name, TRUE);
  g_string_free (parser->style_name, TRUE);
  g_string_free (parser->filename, TRUE);

  for (list = parser->tribes; list; list = list->next)
    g_free (list->data);
  g_slist_free (parser->tribes);
}

static void         
gfm_rc_startElement (GfmParser   *parser,
		     const CHAR  *name,
		     const CHAR **attrs)
{
  const CHAR *aname;
  const CHAR *adata;
  gint i;

  switch (parser->state)
    {
    case GFM_START:
      if (strcmp (name, "fonts"))
	{
	  parser->state = GFM_UNKNOWN;
	  parser->last_state = GFM_START;
	  parser->unknown_depth++;
	}
      else
	{
	  parser->state = GFM_IN_FONTS;
	}
      break;

    case GFM_IN_FONTS:
      if (strcmp (name, "family"))
	{
	  parser->state = GFM_UNKNOWN;
	  parser->last_state = GFM_IN_FONTS;
	  parser->unknown_depth++;
	}
      else
	{
	  for (i = 0; attrs[i]; i++)
	    {
	      aname = attrs[i++];
	      adata = attrs[i];
	      
	      if (strcmp (aname, "name") == 0)
		g_string_assign (parser->family_name, adata);
	    }
	  g_print ("family %s:\n", parser->family_name->str);
	  parser->state = GFM_IN_FAMILY;
	}
      break;
      
    case GFM_IN_FAMILY:
      if (strcmp (name, "style") == 0)
	{
	  for (i = 0; attrs[i]; i++)
	    {
	      aname = attrs[i++], 
		adata = attrs[i];
	      
	      if (strcmp (aname, "name") == 0)
		g_string_assign (parser->style_name, adata);
	    }
	  parser->state = GFM_IN_STYLE;
	}
      else if (strcmp (name, "tribe") == 0)
	{
	  parser->state = GFM_IN_TRIBE;
	}
      else
 	{
	  parser->state = GFM_UNKNOWN;
	  parser->last_state = GFM_IN_FAMILY;
	  parser->unknown_depth++;
	}
      break;

    case GFM_IN_STYLE:
      if (strcmp (name, "file") == 0)
	parser->state = GFM_IN_FILE;
      else if (strcmp (name, "index") == 0)
	parser->state = GFM_IN_INDEX;
      else
	{
	  parser->state = GFM_UNKNOWN;
	  parser->last_state = GFM_IN_STYLE;
	  parser->unknown_depth++;
	}
      break;

    case GFM_FINISH:
      parser->state = GFM_UNKNOWN;
      parser->last_state = GFM_FINISH;
      parser->unknown_depth++;
      break;

    case GFM_UNKNOWN:
      parser->unknown_depth++;
      break;
      
    default:
      g_warning ("Huh? What do I do here?\n");
      break;
    }
}

static void
gfm_rc_endElement (GfmParser  *parser,
		   const CHAR *name)
{
  GSList *list;

  switch (parser->state)
    {
    case GFM_IN_FONTS:
      parser->state = GFM_FINISH;
      break;

    case GFM_IN_FAMILY:
      for (list = parser->tribes; list; list = list->next)
	{
	  g_print (" in tribe %s\n", (gchar *)list->data);
	  g_free (list->data);
	}
      g_slist_free (parser->tribes);
      parser->tribes = NULL;
      g_string_assign (parser->family_name, "");
      parser->state = GFM_IN_FONTS;
      break;
      
    case GFM_IN_STYLE:
      g_print (" style %s:\n", parser->style_name->str);
      g_print ("   %s (%d)\n",  parser->filename->str, parser->index);
      g_string_assign (parser->style_name, "");
      g_string_assign (parser->filename, "");
      parser->index = 0;
      parser->state = GFM_IN_FAMILY;
      break;

    case GFM_IN_FILE:
      g_string_assign (parser->filename, parser->buffer->str);
      g_string_assign (parser->buffer, "");
      parser->state = GFM_IN_STYLE;
      break;
      
    case GFM_IN_INDEX:
      parser->index = atoi (parser->buffer->str);
      g_string_assign (parser->buffer, "");
      parser->state = GFM_IN_STYLE;
      break;

    case GFM_IN_TRIBE:
      parser->tribes = 
	g_slist_prepend (parser->tribes, g_strdup (parser->buffer->str));
      g_string_assign (parser->buffer, "");
      parser->state = GFM_IN_FAMILY;
      break;
      
    case GFM_UNKNOWN:
      parser->unknown_depth--;
      if (parser->unknown_depth == 0)
	parser->state = parser->last_state;
      break;
      
    default:
      g_warning ("Huh? What do I do here?\n");
      break;      
    }
}

static void         
gfm_rc_characters (GfmParser  *parser,
		   const CHAR *chars,
		   gint        len)
{
  gint i;
  
  switch (parser->state)
    {
    case GFM_IN_FILE:
    case GFM_IN_INDEX:  
    case GFM_IN_TRIBE:
      for (i = 0; i < len; i++)
	g_string_append_c (parser->buffer, chars[i]);
      break;

    default:
      break;
    }
}

static void         
gfm_rc_warning (GfmParser   *parser,
		const gchar *msg,
		...)
{
  va_list args;
  
  va_start (args, msg);
  g_logv ("GFM", G_LOG_LEVEL_WARNING, msg, args);
  va_end (args);
}

static void         
gfm_rc_error (GfmParser   *parser,
	      const gchar *msg,
	      ...)
{
  va_list args;
  
  va_start (args, msg);
  g_logv ("GFM", G_LOG_LEVEL_CRITICAL, msg, args);
  va_end (args);
}

static void         
gfm_rc_fatal (GfmParser   *parser,
	      const gchar *msg,
	      ...)
{
  va_list args;
  
  va_start (args, msg);
  g_logv ("GFM", G_LOG_LEVEL_ERROR, msg, args);
  va_end (args);
}



