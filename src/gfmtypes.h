/* GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 * gfmtypes.h
 *   Type declarations
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */

#ifndef __GFM_TYPES_H__
#define __GFM_TYPES_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef struct _GFontManager  GFontManager;
typedef struct _GfmAll        GfmAll;
typedef struct _GfmFace       GfmFace;
typedef struct _GfmFamily     GfmFamily;
typedef struct _GfmTribe      GfmTribe;


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GFM_TYPES_H__ */






