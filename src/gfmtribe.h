/* GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 * GfmTribe
 *   A group of GfmFamilies.
 *
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */

#ifndef __GFM_TRIBE_H__
#define __GFM_TRIBE_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GFM_TYPE_TRIBE            (gfm_tribe_get_type ())
#define GFM_TRIBE(obj)            (GTK_CHECK_CAST ((obj), GFM_TYPE_TRIBE, GfmTribe))
#define GFM_TRIBE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GFM_TYPE_TRIBE, GfmTribeClass))
#define GFM_IS_TRIBE(obj)         (GTK_CHECK_TYPE ((obj), GFM_TYPE_TRIBE))
#define GFM_IS_TRIBE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GFM_TYPE_TRIBE))


typedef struct _GfmTribeClass  GfmTribeClass;

struct _GfmTribe
{
  GtkObject  object;

  gchar     *name; 
  GTree     *families;
};

struct _GfmTribeClass
{
  GtkObjectClass parent_class;

  void (* changed)   (GfmTribe *tribe);
  void (* renamed)   (GfmTribe *tribe);
};  


GtkType     gfm_tribe_get_type      (void);
GtkObject * gfm_tribe_new           (gchar *name);
void        gfm_tribe_rename        (GfmTribe *tribe, gchar *new_name);
void        gfm_tribe_add_family    (GfmTribe *tribe, GfmFamily *family);
void        gfm_tribe_remove_family (GfmTribe *tribe, GfmFamily *family);
GfmFamily * gfm_tribe_lookup_family (GfmTribe *tribe, gchar *family_name);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GFM_TRIBE_H__ */




