/* GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 * testgfm
 *   A test program for the library.
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */

#include <gtk/gtk.h>
#include "gfm.h"

int 
main (int   argc,
      char *argv[])
{
  GFontManager *gfm;
  GfmFace      *face;

  gtk_init (&argc, &argv);

  g_print ("\nCreating a new fontmanager ... ");
  gfm = gfm_new ();
  if (gfm)
    g_print ("OK\n");
  else
    {
      g_print ("failed\n");
      exit (-1);
    }

  g_print ("\n");

  g_print ("Adding agate.pfb.\n");
  gfm_fontfile_add (gfm, "agate.pfb");
  
  g_print ("Looking for \"Agate Regular\" ... ");
  face = gfm_face_lookup (gfm, "Agate", "Regular");
  if (face)
    g_print ("found\n");
  else
    g_print ("NOT found\n");

  g_print ("Looking for \"Agate Bold\" (should fail) ... ");
  face = gfm_face_lookup (gfm, "Agate", "Regular");
  if (face)
    g_print ("not found\n");
  else
    g_print ("FOUND (that shouldn't happen)\n");

  g_print ("Removing agate.pfb.\n");
  gfm_fontfile_remove (gfm, "agate.pfb");
  
  g_print ("Looking for \"Agate Regular\" (should fail) ... ");
  face = gfm_face_lookup (gfm, "Agate", "Regular");
  if (face)
    g_print ("FOUND (that shouldn't happen)\n");
  else
    g_print ("not found\n");

  g_print ("\n");

  g_print ("Loading fontrc gfmrc.xml ... \n");
  gfm_rcfile_load (gfm, "gfmrc.xml");
    
  g_print ("\n");

  g_print ("Exiting.\n\n");
  gtk_object_destroy (GTK_OBJECT (gfm));

  return 0;
}





