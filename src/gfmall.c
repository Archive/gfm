/* GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 * GfmAll
 *   All GfmFamilies.
 *
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */

#include "config.h"

#include <string.h>

#include <gtk/gtk.h>

#include "gfmtypes.h"

#include "gfmall.h"
#include "gfmcommon.h"
#include "gfmfamily.h"


static void gfm_all_destroy                 (GtkObject       *object);
static void gfm_all_family_destroy_callback (GfmFamily       *family,
					     gpointer         data);

enum {
  CHANGE,
  LAST_SIGNAL
};
static guint gfm_all_signals[LAST_SIGNAL] = { 0 };

static GtkObjectClass *parent_class = NULL;


static void 
gfm_all_destroy (GtkObject *object)
{
  GfmAll *all;

  g_return_if_fail (all = GFM_ALL (object));

  g_tree_traverse (all->families, 
		   (GTraverseFunc) gfm_common_unref_func, G_IN_ORDER, 
		   (gpointer)all);
  g_tree_destroy (all->families);
      
  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gfm_all_class_init (GfmAllClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;

  parent_class = gtk_type_class (gtk_object_get_type ());

  gfm_all_signals[CHANGE] = 
    gtk_signal_new ("changed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GfmAllClass, changed),
		    gtk_signal_default_marshaller, GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals (object_class, gfm_all_signals, LAST_SIGNAL);

  class->changed = NULL;

  object_class->destroy = gfm_all_destroy;
}

static void
gfm_all_init (GfmAll *all)
{
  all->families = g_tree_new ((GCompareFunc) strcmp);
}

GtkType
gfm_all_get_type (void)
{
  static guint gfm_all_type = 0;

  if (!gfm_all_type)
    {
      GtkTypeInfo gfm_all_info =
      {
	"GfmAll",
	sizeof (GfmAll),
	sizeof (GfmAllClass),
	(GtkClassInitFunc) gfm_all_class_init,
	(GtkObjectInitFunc) gfm_all_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL
      };

      gfm_all_type = gtk_type_unique (gtk_object_get_type (), &gfm_all_info);
    }

  return gfm_all_type;
}

GfmAll *
gfm_all_new (void)
{
  return gtk_type_new (gfm_all_get_type ());
}

void
gfm_all_add_family (GfmAll    *all,
		    GfmFamily *family)
{
  g_return_if_fail (GFM_IS_ALL (all));
  g_return_if_fail (GFM_IS_FAMILY (family));
  
  if (g_tree_lookup (all->families, family->name))
    return;
  
  gtk_object_ref (GTK_OBJECT (family));
  g_tree_insert (all->families, family->name, family);
  gtk_signal_connect (GTK_OBJECT (family), "destroy",
		      (GtkSignalFunc) gfm_all_family_destroy_callback,
		      (gpointer)all);	      

  gtk_signal_emit (GTK_OBJECT (all), gfm_all_signals[CHANGE]);
}

GfmFamily *
gfm_all_lookup_family (GfmAll *all,
		       gchar  *family_name)
{
  g_return_val_if_fail (GFM_IS_ALL (all), NULL);
  g_return_val_if_fail (family_name != NULL, NULL);

  return (g_tree_lookup (all->families, family_name));
}


/*  private functions  */

static void  
gfm_all_family_destroy_callback (GfmFamily *family,
				 gpointer   data)
{
  GfmAll *all;
  
  g_return_if_fail (all = GFM_ALL (data));
  
  g_tree_remove (all->families, family->name);

  gtk_signal_emit (GTK_OBJECT (all), gfm_all_signals[CHANGE]);
}





