/* GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 * GfmAll
 *   All GfmFamilies.
 *
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */

#ifndef __GFM_ALL_H__
#define __GFM_ALL_H__


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GFM_TYPE_ALL            (gfm_all_get_type ())
#define GFM_ALL(obj)            (GTK_CHECK_CAST ((obj), GFM_TYPE_ALL, GfmAll))
#define GFM_ALL_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GFM_TYPE_ALL, GfmAllClass))
#define GFM_IS_ALL(obj)         (GTK_CHECK_TYPE ((obj), GFM_TYPE_ALL))
#define GFM_IS_ALL_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GFM_TYPE_ALL))


typedef struct _GfmAllClass  GfmAllClass;

struct _GfmAll
{
  GtkObject  object;

  GTree     *families;
};

struct _GfmAllClass
{
  GtkObjectClass parent_class;

  void (* changed)   (GfmAll *all);
};

GtkType     gfm_all_get_type      (void);
GfmAll    * gfm_all_new           (void);
void        gfm_all_add_family    (GfmAll *all, GfmFamily *family);
GfmFamily * gfm_all_lookup_family (GfmAll *all, gchar *family_name);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GFM_ALL_H__ */





