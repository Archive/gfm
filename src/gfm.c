/* GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */
#include "config.h"

#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <gtk/gtk.h>
#include <freetype/freetype.h>

#include "gfmtypes.h"

#include "gfm.h"
#include "gfmall.h"
#include "gfmcommon.h"
#include "gfmface.h"
#include "gfmfamily.h"


static void gfm_destroy            (GtkObject    *object);
static void gfm_face_add           (GFontManager *gfm,
				    FT_Face       ft_face,
				    gchar        *filename,
				    gint          index);
static void gfm_face_remove        (GFontManager *gfm,
				    FT_Face       ft_face,
				    gchar        *filename,
				    gint          index);

static GtkObjectClass *parent_class = NULL;


static void 
gfm_destroy (GtkObject *object)
{
  GFontManager *gfm;

  g_return_if_fail (gfm = GFM (object));

  g_tree_traverse (gfm->tribes, 
		   (GTraverseFunc) gfm_common_unref_func, G_IN_ORDER, 
		   (gpointer)gfm);
  g_tree_destroy (gfm->tribes);

  gtk_object_destroy (GTK_OBJECT (gfm->all));

  if (gfm->library)
    FT_Done_FreeType (gfm->library);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gfm_class_init (GFontManagerClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;

  parent_class = gtk_type_class (gtk_object_get_type ());

  object_class->destroy = gfm_destroy;
}

static void
gfm_init (GFontManager *gfm)
{
  gfm->library = NULL;
  gfm->all     = (GfmAll *)gfm_all_new ();
  gfm->tribes  = g_tree_new ((GCompareFunc) strcmp);
}

GtkType
gfm_get_type (void)
{
  static guint gfm_type = 0;

  if (!gfm_type)
    {
      GtkTypeInfo gfm_info =
      {
	"GFontManager",
	sizeof (GFontManager),
	sizeof (GFontManagerClass),
	(GtkClassInitFunc) gfm_class_init,
	(GtkObjectInitFunc) gfm_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL
      };

      gfm_type = gtk_type_unique (gtk_object_get_type (), &gfm_info);
    }

  return gfm_type;
}

GFontManager *
gfm_new (void)
{
  GFontManager *gfm;
  FT_Error error;

  gfm = gtk_type_new (gfm_get_type ());

  error = FT_Init_FreeType (&gfm->library);
  if (error)
    {
      g_warning ("Couldn't initialize FreeType library");
      gfm_destroy (GTK_OBJECT (gfm));
      return NULL;
    }

  return gfm;
}

void        
gfm_fontfile_add (GFontManager *gfm,
		  gchar        *filename)
{
  struct stat filestat;
  gint i;
  FT_Face  face;
  FT_Error error;

  g_return_if_fail (GFM_IS_GFM (gfm));
  g_return_if_fail (filename != NULL);

  if (stat (filename, &filestat) || access (filename, R_OK))
    {
      g_warning ("cannot read file %s", filename);

      return;
    }

  error = FT_New_Face (gfm->library, filename, 0, &face);
  
  if (!error)
    {
      gfm_face_add (gfm, face, filename, 0);

      for (i = 1; i < face->num_faces; i++)
	{
	  FT_Done_Face (face);
	  error = FT_New_Face (gfm->library, filename, i, &face);
	  if (!error)
	    gfm_face_add (gfm, face, filename, i);
	}
      
      FT_Done_Face (face);
    }
}


void        
gfm_fontfile_remove (GFontManager *gfm,
		     gchar        *filename)
{
  struct stat filestat;
  gint i;
  FT_Face  face;
  FT_Error error;

  g_return_if_fail (GFM_IS_GFM (gfm));
  g_return_if_fail (filename != NULL);

  if (stat (filename, &filestat) || access (filename, R_OK))
    {
      g_warning ("cannot read file %s", filename);

      return;
    }

  error = FT_New_Face (gfm->library, filename, 0, &face);
  
  if (!error)
    {
      gfm_face_remove (gfm, face, filename, 0);
      
      for (i = 1; i < face->num_faces; i++)
	{
	  FT_Done_Face (face);
	  error = FT_New_Face (gfm->library, filename, i, &face);
	  if (!error)
	    gfm_face_remove (gfm, face, filename, i);
	}
      
      FT_Done_Face (face);
    }
}

GfmFace * 
gfm_face_lookup (GFontManager *gfm, 
		 gchar        *family_name,
		 gchar        *style_name)
{
  GfmFamily *family;

  g_return_val_if_fail (GFM_IS_GFM (gfm), NULL);
  g_return_val_if_fail (family_name != NULL, NULL);
  g_return_val_if_fail (style_name != NULL, NULL);
  
  family = gfm_all_lookup_family (gfm->all, family_name);

  if (!family)
    return NULL;
  
  return gfm_family_lookup_face (family, style_name);
}



/*  private functions  */

static void
gfm_face_add (GFontManager *gfm,
	      FT_Face       ft_face,
	      gchar        *filename,
	      gint          index)
{
  GfmFamily *family;

  family = gfm_all_lookup_family (gfm->all, (gchar *)ft_face->family_name);

  if (!family)
    family = gfm_family_new (gfm->all, (gchar *)ft_face->family_name);

  if (gfm_family_lookup_face (family, ft_face->style_name) == NULL)
    gfm_face_new (family, (gchar *)ft_face->style_name, filename, index);
}

static void
gfm_face_remove (GFontManager *gfm,
		 FT_Face       ft_face,
		 gchar        *filename,
		 gint          index)
{
  GfmFace   *face;
  GfmFamily *family;

  family = gfm_all_lookup_family (gfm->all, (gchar *)ft_face->family_name);

  if (!family)
    return;
  
  face = gfm_family_lookup_face (family, ft_face->style_name);

  /* FIXME:
      This is dangerous! The object might still be in use 
      outside the font_manager. Should go through all tribes
      and GfmAll and unref it instead. 
   */
  if (face)
    gtk_object_destroy (GTK_OBJECT (face));
}



