/* GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 * GfmFamily
 *   A family of faces.
 *
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */

#ifndef __GFM_FAMILY_H__
#define __GFM_FAMILY_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GFM_TYPE_FAMILY            (gfm_family_get_type ())
#define GFM_FAMILY(obj)            (GTK_CHECK_CAST ((obj), GFM_TYPE_FAMILY, GfmFamily))
#define GFM_FAMILY_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GFM_TYPE_FAMILY, GfmFamilyClass))
#define GFM_IS_FAMILY(obj)         (GTK_CHECK_TYPE ((obj), GFM_TYPE_FAMILY))
#define GFM_IS_FAMILY_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GFM_TYPE_FAMILY))


typedef struct _GfmFamilyClass  GfmFamilyClass;

struct _GfmFamily
{
  GtkObject  object;

  gchar     *name;    /* the family_name    */
  GTree     *faces;   /* a list of GfmFaces */
};

struct _GfmFamilyClass
{
  GtkObjectClass parent_class;

  void (* changed)   (GfmFamily *family);
};


GtkType     gfm_family_get_type    (void);
GfmFamily * gfm_family_new         (GfmAll *all, gchar *family_name);
void        gfm_family_add_face    (GfmFamily *family, GfmFace *face);
void        gfm_family_remove_face (GfmFamily *family, GfmFace *face);
GfmFace   * gfm_family_lookup_face (GfmFamily *family, gchar *style_name);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GFM_FAMILY_H__ */




