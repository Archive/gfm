/* GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 * GfmFamily
 *   A family of faces.
 *
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */

#include "config.h"

#include <string.h>

#include <gtk/gtk.h>

#include "gfmtypes.h"

#include "gfmall.h"
#include "gfmcommon.h"
#include "gfmface.h"
#include "gfmfamily.h"


static void gfm_family_destroy               (GtkObject       *object);
static void gfm_family_face_destroy_callback (GfmFace         *face,
					      gpointer         data);

enum {
  CHANGE,
  LAST_SIGNAL
};
static guint gfm_family_signals[LAST_SIGNAL] = { 0 };

static GtkObjectClass *parent_class = NULL;


static void 
gfm_family_destroy (GtkObject *object)
{
  GfmFamily *family;

  g_return_if_fail (family = GFM_FAMILY (object));

  g_tree_traverse (family->faces, 
		   (GTraverseFunc) gfm_common_unref_func, G_IN_ORDER, 
		   (gpointer)family);
  g_tree_destroy (family->faces);

  g_free (family->name);
      
  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gfm_family_class_init (GfmFamilyClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;

  parent_class = gtk_type_class (gtk_object_get_type ());

  gfm_family_signals[CHANGE] = 
    gtk_signal_new ("changed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GfmFamilyClass, changed),
		    gtk_signal_default_marshaller, GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals (object_class, gfm_family_signals, LAST_SIGNAL);

  class->changed = NULL;
  
  object_class->destroy = gfm_family_destroy;
}

static void
gfm_family_init (GfmFamily *family)
{
  family->name  = NULL;
  family->faces = g_tree_new ((GCompareFunc) strcmp);
}

GtkType
gfm_family_get_type (void)
{
  static guint gfm_family_type = 0;

  if (!gfm_family_type)
    {
      GtkTypeInfo gfm_family_info =
      {
	"GfmFamily",
	sizeof (GfmFamily),
	sizeof (GfmFamilyClass),
	(GtkClassInitFunc) gfm_family_class_init,
	(GtkObjectInitFunc) gfm_family_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL
      };

      gfm_family_type = gtk_type_unique (gtk_object_get_type (), &gfm_family_info);
    }

  return gfm_family_type;
}

GfmFamily *
gfm_family_new (GfmAll *all,
		gchar  *name)
{
  GfmFamily *family;
  
  g_return_val_if_fail (GFM_IS_ALL (all), NULL);
  g_return_val_if_fail (name != NULL, NULL);

  family = gtk_type_new (gfm_family_get_type ());
  family->name = g_strdup (name);

  gfm_all_add_family (all, family);

  return family;
}

void
gfm_family_add_face (GfmFamily *family,
		     GfmFace   *face)
{
  g_return_if_fail (GFM_IS_FAMILY (family));
  g_return_if_fail (GFM_IS_FACE (face));
  
  if (g_tree_lookup (family->faces, face->style_name))
    return;  /*  FIXME: should handle this case  */

  gtk_object_ref (GTK_OBJECT (face));
  face->family = GTK_OBJECT (family);
  g_tree_insert (family->faces, face->style_name, face);
  gtk_signal_connect (GTK_OBJECT (face), "destroy",
		      (GtkSignalFunc)gfm_family_face_destroy_callback,
		      (gpointer)family);	      

  gtk_signal_emit (GTK_OBJECT (family), gfm_family_signals[CHANGE]);
}

void
gfm_family_remove_face (GfmFamily *family,
			GfmFace   *face)
{
  g_return_if_fail (GFM_IS_FAMILY (family));
  g_return_if_fail (GFM_IS_FACE (face));
  
  g_tree_remove (family->faces, face->style_name);
  face->family = NULL;
  gtk_object_unref (GTK_OBJECT (face));

  if (family->faces == NULL)
    gfm_family_destroy (GTK_OBJECT (family));
  else
    gtk_signal_emit (GTK_OBJECT (family), gfm_family_signals[CHANGE]);
}

GfmFace *
gfm_family_lookup_face (GfmFamily *family,
			gchar     *style_name)
{
  g_return_val_if_fail (GFM_IS_FAMILY (family), NULL);
  g_return_val_if_fail (style_name != NULL, NULL);

  return (g_tree_lookup (family->faces, style_name));
}

/*  private functions  */

static void  
gfm_family_face_destroy_callback (GfmFace  *face,
				  gpointer  data)
{
  GfmFamily *family;
  
  g_return_if_fail (family = GFM_FAMILY (data));
  
  g_tree_remove (family->faces, face->style_name);

  if (family->faces == NULL)
    gfm_family_destroy (GTK_OBJECT (family));
  else
    gtk_signal_emit (GTK_OBJECT (family), gfm_family_signals[CHANGE]);
}

