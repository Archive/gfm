/* GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 * GfmTribe
 *   A group of GfmFamilies.
 *
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */

#include "config.h"

#include <string.h>

#include <gtk/gtk.h>

#include "gfmtypes.h"

#include "gfmcommon.h"
#include "gfmfamily.h"
#include "gfmtribe.h"


static void gfm_tribe_destroy                 (GtkObject *object);
static void gfm_tribe_family_destroy_callback (GfmFamily *family,
					       gpointer   data);

enum {
  CHANGE,
  RENAME,
  LAST_SIGNAL
};

static guint gfm_tribe_signals[LAST_SIGNAL] = { 0 };

static GtkObjectClass *parent_class = NULL;


static void 
gfm_tribe_destroy (GtkObject *object)
{
  GfmTribe *tribe;

  g_return_if_fail (tribe = GFM_TRIBE (object));

  g_tree_traverse (tribe->families, 
		   (GTraverseFunc) gfm_common_unref_func, G_IN_ORDER, 
		   (gpointer)tribe);
  g_tree_destroy (tribe->families);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gfm_tribe_class_init (GfmTribeClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;

  parent_class = gtk_type_class (gtk_object_get_type ());

  gfm_tribe_signals[CHANGE] = 
    gtk_signal_new ("changed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GfmTribeClass, changed),
		    gtk_signal_default_marshaller, GTK_TYPE_NONE, 0);
  gfm_tribe_signals[RENAME] = 
    gtk_signal_new ("renamed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GfmTribeClass, renamed),
		    gtk_signal_default_marshaller, GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals (object_class, gfm_tribe_signals, LAST_SIGNAL);

  class->changed = NULL;
  class->renamed = NULL;

  object_class->destroy = gfm_tribe_destroy;
}

static void
gfm_tribe_init (GfmTribe *tribe)
{
  tribe->name     = NULL;
  tribe->families = g_tree_new ((GCompareFunc) strcmp);
}

GtkType
gfm_tribe_get_type (void)
{
  static guint gfm_tribe_type = 0;

  if (!gfm_tribe_type)
    {
      GtkTypeInfo gfm_tribe_info =
      {
	"GfmTribe",
	sizeof (GfmTribe),
	sizeof (GfmTribeClass),
	(GtkClassInitFunc) gfm_tribe_class_init,
	(GtkObjectInitFunc) gfm_tribe_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL
      };

      gfm_tribe_type = gtk_type_unique (gtk_object_get_type (), &gfm_tribe_info);
    }

  return gfm_tribe_type;
}

GtkObject *
gfm_tribe_new (gchar *name)
{
  GfmTribe *tribe;
  
  g_return_val_if_fail (name != NULL, NULL);

  tribe = gtk_type_new (gfm_tribe_get_type ());
  tribe->name = g_strdup (name);

  return GTK_OBJECT (tribe);
}

void
gfm_tribe_rename (GfmTribe *tribe,
		  gchar    *new_name)
{
  g_return_if_fail (GFM_IS_TRIBE (tribe));
  g_return_if_fail (new_name != NULL);
  
  g_free (tribe->name);
  tribe->name = g_strdup (new_name);
  
  gtk_signal_emit (GTK_OBJECT (tribe), gfm_tribe_signals[RENAME]);
}

void
gfm_tribe_add_family (GfmTribe  *tribe,
		      GfmFamily *family)
{
  g_return_if_fail (GFM_IS_TRIBE (tribe));
  g_return_if_fail (GFM_IS_FAMILY (family));
  
  if (g_tree_lookup (tribe->families, family->name))
    return;
  
  gtk_object_ref (GTK_OBJECT (family));
  g_tree_insert (tribe->families, family->name, family);
  gtk_signal_connect (GTK_OBJECT (family), "destroy",
		      (GtkSignalFunc)gfm_tribe_family_destroy_callback,
		      (gpointer)tribe);

  gtk_signal_emit (GTK_OBJECT (tribe), gfm_tribe_signals[CHANGE]);  
}

void
gfm_tribe_remove_family (GfmTribe  *tribe,
			 GfmFamily *family)
{
  g_return_if_fail (GFM_IS_TRIBE (tribe));
  g_return_if_fail (GFM_IS_FAMILY (family));
  
  g_tree_remove (tribe->families, family->name);
  gtk_signal_disconnect_by_data (GTK_OBJECT (family), (gpointer)tribe);
  gtk_object_unref (GTK_OBJECT (family));

  gtk_signal_emit (GTK_OBJECT (tribe), gfm_tribe_signals[CHANGE]);
}

GfmFamily *
gfm_tribe_lookup_family (GfmTribe *tribe,
			 gchar    *family_name)
{
  g_return_val_if_fail (GFM_IS_TRIBE (tribe), NULL);
  g_return_val_if_fail (family_name != NULL, NULL);
  
  return (g_tree_lookup (tribe->families, family_name));
}


/*  private functions  */

static void  
gfm_tribe_family_destroy_callback (GfmFamily *family,
				   gpointer   data)
{
  GfmTribe *tribe;
  
  g_return_if_fail (tribe = GFM_TRIBE (data));
  
  g_tree_remove (tribe->families, family->name);
  gtk_signal_emit (GTK_OBJECT (tribe), gfm_tribe_signals[CHANGE]);
}






