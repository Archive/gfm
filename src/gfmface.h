/* GNU Font Manager 
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *
 * GfmFace
 *   An object describing some properties of a face and the location
 *   of the related font file.
 *
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */

#ifndef __GFM_FACE_H__
#define __GFM_FACE_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GFM_TYPE_FACE            (gfm_face_get_type ())
#define GFM_FACE(obj)            (GTK_CHECK_CAST ((obj), GFM_TYPE_FACE, GfmFace))
#define GFM_FACE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GFM_TYPE_FACE, GfmFaceClass))
#define GFM_IS_FACE(obj)         (GTK_CHECK_TYPE ((obj), GFM_TYPE_FACE))
#define GFM_IS_FACE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GFM_TYPE_FACE))


typedef struct _GfmFaceClass  GfmFaceClass;

struct _GfmFace
{
  GtkObject  object;
  
  gchar     *style_name;  /* a name describing the face style      */
  gchar     *file_name;   /* full path to the font_file            */
  gint       index;       /* face index in the font file           */ 
  
  /* private */
  GtkObject *family;      /* the parent family                     */
};

struct _GfmFaceClass
{
  GtkObjectClass parent_class;
};  

GtkType   gfm_face_get_type (void);
GfmFace * gfm_face_new      (GfmFamily *family, 
			     gchar     *style_name, 
			     gchar     *file_name, 
			     gint       index);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GFM_FACE_H__ */




